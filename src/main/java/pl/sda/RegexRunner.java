package pl.sda;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class RegexRunner {
    private final static String METEO_STRING = "Poniedzialek;15C wiatr wschod mocny\n" +
            "Wtorek}16C wiatr zachod mocny\n" +
            "Sroda[15C wiatr brak brak\n" +
            "Czwartek!9C wiatr polnoc sredni\n";

    private final static List<String> TAXI_STRINGS = Arrays.asList(
            "Poprosze taksowke na Dworzec",
            "Taksowka pod Galerie szybko",
            "Gdzie ta taksowka?Miala byc 30 minut temu na Sokolskiej",
            "Platnosc karta za 3 minuty w Supersamie dziekuje");

    private final static String NUMBERS_WITH_SOME_SELECTORS = "1;2/3:4#5$63@1^3^8$21";

    private final static List<String> FILENAMES = Arrays.asList("alamakota.txt", "log-12-02-2019.txt", "fotkizwakacji.jpg", "memy.png", "smiesznekoty.gif", "wirus.exe", "innyplik.java", "nienazywajtakklas.java", "krytyczne-11-03-2018.log");

    private final static String SOME_CODE = "private int i = 0;\n" +
            "public String abc = \"aaa\";\n" +
            "protected float mojaMetoda() {\n" +
            "\n" +
            "}\n" +
            "public double jakasZmienna = 2.3d;";
    public static final Pattern EXTENSION_PATTERN = Pattern.compile("\\w+\\.(\\w+)");

    public void run() {
        //zadanie 3
        String[] numbersString = NUMBERS_WITH_SOME_SELECTORS.split("\\D");

        Pattern digitPattern = Pattern.compile("(\\d+)");
        Matcher matcher = digitPattern.matcher(NUMBERS_WITH_SOME_SELECTORS);
        int summedUsingMatcher = 0;
        while (matcher.find()) {
            System.out.println();
            summedUsingMatcher += Integer.valueOf(matcher.group(1));
        }
        System.out.println(summedUsingMatcher);

        int summedUsingStream = Arrays.stream(numbersString)
                .mapToInt(arrElement -> Integer.parseInt(arrElement))
                .sum();
        System.out.println(summedUsingStream);
        int sum = 0;
        for (String stringNumber : numbersString) {
            sum += Integer.parseInt(stringNumber);
        }
        System.out.println(sum);

        //2
        //"Gdzie ta taksowka?Miala byc 30 minut temu na Sokolskiej",

        for (String orderText : TAXI_STRINGS) {
            String taxiMessage = orderText.replaceAll(".*(?:na|pod|w) (\\w+).*", "Zamowienie na $1");
            System.out.println(taxiMessage);
        }

        //zadanie 1

        Pattern meteoPattern = Pattern.compile("(\\w+)\\W(\\d+)C wiatr (\\w+) (\\w+)");
        Matcher meteoMatcher = meteoPattern.matcher(METEO_STRING);
        double sumOfReadings = 0;
        int readingsCounter = 0;
        while (meteoMatcher.find()) {
            String dayOfWeek = meteoMatcher.group(1);
            double temperature = Double.parseDouble(meteoMatcher.group(2));
            String windDirection = meteoMatcher.group(3);
            String windForce = meteoMatcher.group(4);

            sumOfReadings += temperature;
            readingsCounter++;
            System.out.println("w " + dayOfWeek + " " + windDirection);
            System.out.printf("W %s wiało %s z %s%n", dayOfWeek, windForce, windDirection);
        }
        double average = sumOfReadings / readingsCounter;
        System.out.println(average);

        //zadanie 4

        String codeWithPrivate = SOME_CODE.replaceAll("(public|protected)", "private");
        String codeWithFieldsAsBigDecimal =
                SOME_CODE.replaceAll("(\\w+) \\w+ (\\w+.*?);", "$1 BigDecimal $2;");

        System.out.println(codeWithPrivate);
        System.out.println(codeWithFieldsAsBigDecimal);

        //5.a
        int javaFileCounter = 0;
        int yCounter = 0;
        Map<String, Integer> fileExtensionOccurenceMap = new HashMap<>();

        for (String filename : FILENAMES) {
            boolean isJavaFile = filename.matches("\\w+\\.java");
            boolean isFileWithDatePattern = filename.matches("\\w+-[0123]?\\d-[01]?\\d-\\d{4}\\.\\w+");
            boolean isFilenameWithY = filename.matches(".*y.*");

            Matcher extensionMatcher = EXTENSION_PATTERN.matcher(filename);
            if (extensionMatcher.find()) {
                String extension = extensionMatcher.group(1);
                Integer currentExtensionAmount = fileExtensionOccurenceMap.getOrDefault(extension, 0);
                currentExtensionAmount++;
                fileExtensionOccurenceMap.put(extension, currentExtensionAmount);
            } else {
                System.out.println("Niepoprawna nazwa pliku");
            }

            if (isFilenameWithY) {
                yCounter++;
            }
            if (isFileWithDatePattern) {
                System.out.println(filename);
            }
            if (isJavaFile) {
                javaFileCounter++;
            }
        }

        /*Set<Map.Entry<String, Integer>> entries = fileExtensionOccurenceMap.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }*/
        Map<String, List<String>> collect = FILENAMES
                .stream()
                .map(filename -> EXTENSION_PATTERN.matcher(filename))
                .filter(extMatcher -> extMatcher.find())
                .map(extMatcher -> extMatcher.group(1))
                .collect(Collectors.groupingBy(e -> e));

        for (Map.Entry<String, List<String>> extensionEntry : collect.entrySet()) {
            System.out.println(extensionEntry.getKey() + ":" + extensionEntry.getValue().size());
        }


        System.out.println("Plikow z y w nazwie jest: " + yCounter);
        System.out.println("Plikow z rozszerzeniem Java: " + javaFileCounter);
    }
}
